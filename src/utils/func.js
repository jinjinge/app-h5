function pluralize (time, label) {
  return time + label + '前'
}

export function timeAgo (time) {
  const between = (+Date.now() - +new Date(time)) / 1000
  if (between < 60) {
    return pluralize(~~(between), ' 秒')
  } else if (between < 3600) {
    return pluralize(~~(between / 60), ' 分钟')
  } else if (between < 86400) {
    return pluralize(~~(between / 3600), ' 小时')
  } else if (between < (86400 * 30)) {
    return pluralize(~~(between / 86400), ' 天')
  } else if (between < (86400 * 30 * 12)) {
    return pluralize(~~(between / (86400 * 30)), '个月')
  } else {
    return pluralize(~~(between / (86400 * 30 * 12)), '年')
  }
}

/* 日期格式化 */
export function formatDate (date, fmt) {
  date = new Date(date)
  fmt = fmt || 'yyyy-MM-dd hh:mm'
  const o = {
    'M+': date.getMonth() + 1, // 月份
    'd+': date.getDate(), // 日
    'H+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    S: date.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (const k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}

// 判断字符串是否为空
export function isEmpty (str) {
  if (/^\s*$/.test(str) === true) {
    return true
  }
}

// https://www.cnblogs.com/zpblogs/p/11239597.html
//检查号码是否符合中国二代身份证规范，包括长度，类型
export function isIdCardNo (cardNo) {
  if (isEmpty(cardNo)) {
    return false
  }
  //这个代码表示身份证可以为空
  //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
  var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/
  if (reg.test(cardNo) === false) {
    return false
  }
  return true
}
