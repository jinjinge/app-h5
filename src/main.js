import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'normalize.css'
import 'mand-mobile/components/_style/global.styl'
import VueWechatTitle from 'vue-wechat-title'

Vue.config.productionTip = false
Vue.use(VueWechatTitle)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
