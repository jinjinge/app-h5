import request from "@utils/request"

/**
 * 分享
 * @returns {*}
 */
export function getShare() {
  return request.get("/share", {}, { login: false })
}

/**
 * 获取微信sdk配置
 * @returns {*}
 */
export function getWechatConfig() {
  return request.get(
    "/wechat/config",
    { url: document.location.href },
    { login: false }
  )
}

export function getWechatAppId() {
  return request.get(
    "/wechat/getAppId",
    {},
    { login: false }
  )
}

/**
 * 获取微信sdk配置
 * @returns {*}
 */
export function wechatAuth(code, spread, loginType) {
  return request.get(
    "/wechat/auth",
    { code, spread, loginType },
    { login: false }
  )
}

/**
 * 获取图片base64
 * @retins {*}
 * */
export function imageBase64(image, code) {
  return request.post(
    "/image_base64",
    { image: image, code: code },
    { login: false }
  )
}
