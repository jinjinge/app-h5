import request from '@utils/request'

/**
 * 用户登录
 * @param data object 用户账号密码
 */
export function login (data) {
  return request.post('/auth/login-pwd', data, { login: false })
}

/**
 * 用户微信公众号登录
 */
export function loginWxMp (appId, code, state) {
  return request.get(`/auth/login-wxmp/${appId}?code=${code}&state=${state}`, {}, { login: false })
}

/*
 * 用户信息
 */
export function getUserInfo () {
  return request.get('/auth/userinfo', {}, { login: true })
}

/*
 * 退出登录
 * */
export function logout() {
  return request.post("/auth/logout")
}
