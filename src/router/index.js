import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Auth from "@views/Auth"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'APP-首页'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@views/Login.vue'),
    meta: {
      title: 'APP-登录'
    }
  },
  {
    path: '/relogin',
    name: 'Relogin',
    component: () => import('@views/Relogin.vue'),
    meta: {
      title: 'APP-重新登录'
    }
  },
  {
    path: '/logout',
    name: 'Logout',
    component: () => import('@views/Logout.vue'),
    meta: {
      title: 'APP-退出'
    }
  },
  {
    path: "/auth/:url",
    name: "Auth",
    meta: {
      title: "APP-鉴权"
    },
    component: Auth
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('@views/Profile.vue'),
    meta: {
      title: 'APP-我的'
    }
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('@views/NotFound.vue'),
    meta: {
      title: '页面未找到'
    }
  }
]

const router = new VueRouter({
  // hash
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

console.log('process.env.BASE_URL=' + process.env.BASE_URL)

export default router
