# APP-H5
Coadmin 的 H5 网页演示工程，对接Coadmin中app-api项目下的api接口。

Coadmin：

     https://gitee.com/jinjinge/coadmin
     https://github.com/jinjingmail/coadmin

使用 mand-mobile 2.x 作为UI框架：

     https://didi.github.io/mand-mobile/#/zh-CN/home

如果是在普通浏览器里面打开，则使用用户名密码登录。

如果是在微信里面打开，则使用微信openid登录：

     申请微信公众号测试账号的方法：
     https://www.cnblogs.com/yimiyan/p/6594205.html
     https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login

## Project setup
```
安装依赖
npm config get registry  #查看当前的仓库源
npm config set registry http://registry.npm.taobao.org  #设置全局仓库源
npm i node-sass --sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
npm install
```

### Compiles and hot-reloads for development
```
运行项目
npm run serve               # 运行在默认端口
npm run serve -- --port 80  # 运行在80端口
```

### Compiles and minifies for production
```
发布项目
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# BUG
